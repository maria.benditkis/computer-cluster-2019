from threading import Thread, Lock
from client import *
import socket
import time
import pickle
import sys
from PyQt4 import QtGui
import logging


class Server(object):
    def __init__(self, app, files_list, computers_table, num_threads_computers, num_threads_programs):
        self.app = app
        self.my_threads = {}
        self.num_threads_computers = num_threads_computers
        self.num_threads_programs = num_threads_programs

        self.BUFSIZE = 1024

        self.files_list = files_list
        self.computers_table, self.comp_addresses = computers_table, []
        self.global_tables = {}
        self.my_clients = AllClients(self.BUFSIZE)

        host = socket.gethostbyname(socket.gethostname())
        port = self.get_open_port()
        self.address = (host, port)
        print 'MY ADDRESS ' + str(self.address)
        self.num_threads_computers[host] = 0
        self.server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.server_socket.bind(self.address)
        self.server_socket.listen(20)

        self.stop_server = False
        self.server_thread = Thread(target=self.start_server)
        self.server_thread.setDaemon(True)
        self.server_thread.start()
        print 'started server'

        self.listener = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)  # UDP
        self.listener.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
        self.listener.bind(("", 64000))
        address_listener = Thread(target=self.listen_udp)
        address_listener.setDaemon(True)
        address_listener.start()

        connect = Thread(target=lambda: self.send_my_address(1, 1))
        connect.setDaemon(True)
        connect.start()

    # a thread that receives new connections
    def start_server(self):
        while not self.stop_server:
            # print 'before try'
            try:
                # print 'before accept ' + str(self.address)
                tcp_client_socket, client_address = self.server_socket.accept()
                # print 'after accept'
                # print '...connected to client:' + str(client_address)
                new_connection = Thread(target=self.handler, args=(tcp_client_socket, client_address))
                new_connection.setDaemon(True)
                new_connection.start()
            except socket.timeout:
                pass

    @staticmethod
    # Returns an open port.
    def get_open_port():
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.bind(("", 0))
        s.listen(1)
        port = s.getsockname()[1]
        s.close()
        return port

    # waits for new computers to connect.
    def listen_udp(self):
        while not self.stop_server:
            data, address = self.listener.recvfrom(1024)
            if not data:
                break
            # print 'data from %s: %s' % (address, data)
            # try:
            parts = data.split(',,, ')
            if parts[0] == 'my address':
                server_address = (parts[1], int(parts[2]))
                if (server_address != self.address) and (server_address[0] not in self.my_clients.clients.keys()):
                    self.num_threads_computers[str(server_address[0])] = 0
                    self.my_clients.add_client(server_address)
                    self.my_clients.clients[server_address[0]].send_message('add me ::: %s,,, %d' % self.address)
                    # print 'address not in all addresses'
                    self.add_to_computers_table(server_address)
                    # print 'added address ' + str(server_address)
            # except Exception, e:
            #     print e
        self.listener.close()
        print 'closed'

    # broadcast server's address
    def send_my_address(self, attempts, waiting_time):
        server = socket.socket(socket.AF_INET, socket.SOCK_DGRAM, socket.IPPROTO_UDP)
        server.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
        server.settimeout(0.2)
        message = b'my address,,, %s,,, %d' % self.address
        while attempts and not self.stop_server:
            attempts -= 1
            server.sendto(message, ('<broadcast>', 64000))
            # print("message sent!")
            time.sleep(waiting_time)

    # adds a server's address to computer's table
    def add_to_computers_table(self, address):
        row = self.computers_table.rowCount()
        self.computers_table.insertRow(row)
        self.computers_table.setItem(row, 0, QtGui.QTableWidgetItem(address[0]))
        self.computers_table.setItem(row, 1, QtGui.QTableWidgetItem(str(address[1])))
        self.computers_table.setItem(row, 2, QtGui.QTableWidgetItem('0'))
        print 'self.computers_table.item(row, 2).text() ' + str(self.computers_table.item(row, 2).text())

    # Thread function that handles the connection with a client
    def handler(self, client_socket, address):
        while True:
            try:
                logger.info(' before server main recv')
                data = client_socket.recv(self.BUFSIZE)
                logger.info('data from ' + str(address) + ':  ' + repr(data))
                # print 'data from ' + str(address) + ':  ' + repr(data)
            except socket.error, e:
                logger.error("socket.error: " + str(address) + ' ' + str(e))
                break

            if not data:
                # print "ending communication with ", address
                break

            command = data.split(' ::: ')[0]
            logger.info('command ' + command)
            args = data.split(' ::: ')[1].split(',,, ')

            if command == 'long message':
                client_socket.send('ok')
                message = self.get_long_message(client_socket, int(args[0]))
                command = message.split(' ::: ')[0]
                args = message.split(' ::: ')[1].split(',,, ')

            if command == 'add me':
                server_address = (args[0], int(args[1]))
                self.num_threads_computers[args[0]] = 0
                self.my_clients.add_client(server_address)
                self.add_to_computers_table(server_address)
                client_socket.send('ok')

            elif command == 'remove me':
                self.my_clients.remove_client((args[0], int(args[1])))
                client_socket.send('ok')

            elif command == 'new worker':
                # print 'command is new worker!'
                self.update_num_threads(args[0], args[1], 1)
                client_socket.send('ok')

            elif command == 'end worker':
                # print 'command is end worker!'
                self.update_num_threads(args[0], args[1], -1)
                client_socket.send('ok')

            elif command == 'binary file':
                # logger.debug(' b s')
                client_socket.send('ok')
                # logger.debug(' a s')
                self.get_binary_file(client_socket, args[0], int(args[1]))
                self.files_list.addItem(args[0])

            elif command == 'import':
                exec('import ' + args[0])
                if args[0] in sys.modules:
                    reload(eval(args[0]))
                # logger.debug(' b s')
                client_socket.send('ok')
                # logger.debug(' a s')

            elif command == 'tell others':
                self.my_clients.send_to_everyone(args[0] + ' ::: ' + ',,, '.join(args[1:]))
                # logger.debug(' b s')
                client_socket.send('ok')
                # logger.debug(' a s')

            elif command == 'call function in thread':
                target_args = []
                if args[2] != '':
                    arguments = args[2].split(',, ')
                    for arg in arguments:
                        target_args += [pickle.loads(arg)]
                target_args = tuple(target_args)
                new_thread = Thread(target=self.worker, args=(client_socket, args[0], args[1], target_args))
                new_thread.setDaemon(True)
                new_thread.start()
                if args[0] in self.my_threads.keys():
                    self.my_threads[args[0]] += [new_thread]

            elif command == 'change global':
                exec('import ' + args[0])
                eval(args[0]).change_global(args[1], pickle.loads(args[2]))
                # logger.debug(' b s')
                client_socket.send('ok')
                # logger.debug(' a s')

            elif command == 'change in global':
                exec('import ' + args[0])
                eval(args[0]).change_in_global(args[1], pickle.loads(args[2]),
                                               pickle.loads(args[3]))
                # logger.debug(' b s')
                client_socket.send('ok')
                # logger.debug(' a s')

            else:
                logger.warning('command: ' + command + '!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!')
        logger.info('handler exit')

    # receives a message that is longer than the buffsize
    def get_long_message(self, client_socket, length):
        message = bytearray()
        while length > 0:
            # logger.debug(' b r')
            message_part = client_socket.recv(self.BUFSIZE)
            message += message_part
            # logger.debug(' a r')
            length -= len(message_part)
        return str(message)

    # receives and saves a binary file
    def get_binary_file(self, client_socket, file_path, file_length):
        # print 'file_length ' + str(file_length)
        binary_array = bytearray()
        while file_length > 0:
            # logger.debug(' b r')
            data = client_socket.recv(self.BUFSIZE)
            # logger.debug(' a r')
            binary_array += data
            file_length -= len(data)
        with open(file_path, 'wb') as binary_file:
            binary_file.write(binary_array)

    # a thread that calls a function
    def worker(self, client_socket, path, function_name, target_args):
        logger.info('start worker ' + function_name)
        self.update_num_threads(str(self.address[0]), path, 1)
        self.my_clients.send_to_everyone('new worker ::: %s,,, %s' % (str(self.address[0]), path))
        exec('import ' + path)
        if function_name == 'main' and path in sys.modules:
            reload(eval(path))
        if function_name == 'main':
            eval(path + '.set_important_variables')(self.global_tables.get(path))
        try:
            logger.debug('before call function ' + function_name)
            eval(path + '.call_function')(self, function_name, target_args)
            logger.debug('after call function ' + function_name)
        except Exception, error:
            error_type = sys.exc_info()[0].__name__
            if error_type != socket.error:
                thread_name = threading.current_thread().name
                self.app.responses[thread_name] = None
                self.app.showMessageBox.emit(thread_name, 'Warning', 'Error', str(error_type), str(error))
                while self.app.responses[thread_name] is None:
                    time.sleep(0.1)
        if client_socket:
            # logger.debug(' b s')
            client_socket.send('ok')
            # logger.debug(' a s')
        self.my_clients.send_to_everyone('end worker ::: %s,,, %s' % (str(self.address[0]), path))
        self.update_num_threads(str(self.address[0]), path, -1)
        logger.info('end worker ' + function_name)

    # updates number of threads for a computer
    def update_num_threads(self, ip, name, add):
        with threads_computers_lock:
            self.num_threads_computers[ip] += add
        if name in self.num_threads_programs.keys():
            with threads_programs_lock:
                self.num_threads_programs[name] += add


logger = logging.getLogger(__name__)
threads_computers_lock = Lock()
threads_programs_lock = Lock()
