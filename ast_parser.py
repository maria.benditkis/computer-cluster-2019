import ast
import astor


class TreeVisitor(ast.NodeTransformer):
    assign_count = 0
    my_functions = []
    first_assigns = []
    global_first_assigns = []

    # :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: visit
    def visit(self, node):
        # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ print node
        if type(node) not in (ast.alias, ast.arguments, ast.Name, ast.Param, ast.Store, ast.Num, ast.Str, ast.Load,
                              ast.keyword, ast.List):
            # print str(node) + str(ast.dump(node))
            pass

        # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ create main function
        if type(node) == ast.Module:
            self.my_functions = self.get_my_functions(node)
            print self.my_functions
            self.first_assigns, self.global_first_assigns = self.create_main_function(node)
            # print 'global_first_assigns: ' + str(self.global_first_assigns)
            pass

        # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ edit functions
        elif type(node) == ast.FunctionDef:
            FunctionChanger(node.name).visit(node)
            node.args.args = [ast.Name(id='my_server', ctx=ast.Param())] + node.args.args
            self.edit_functions(node, node.name)
            pass

        # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ change to milestone1
        elif self.check_thread_assign(node):
            node.func.id = 'ClusterThread'
            pass
        # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ add 'my_server' argument
        elif (type(node) == ast.Call) and \
                (type(node.func) == ast.Name) and \
                (node.func.id in self.my_functions):
            node.args = [ast.Name(id='my_server', ctx=ast.Load())] + node.args
            pass

        # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        ast.fix_missing_locations(node)

        method = 'visit_' + node.__class__.__name__
        visitor = getattr(self, method, self.generic_visit)
        return visitor(node)

    # :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: create_main_function
    def create_main_function(self, node):
        main_function = ast.FunctionDef(name='main', args=ast.arguments(args=[], vararg=None, kwarg=None, defaults=[]),
                                        body=[], decorator_list=[])
        remove_nodes = []
        leave_nodes = []
        first_assigns = []
        added_nodes = []

        for sub_node in node.body:
            nodes_to_add = []
            if type(sub_node) in (ast.FunctionDef, ast.Import, ast.ImportFrom):
                leave_nodes += [sub_node]
            elif type(sub_node) == ast.Assign:
                if type(sub_node.targets[0]) == ast.Tuple:
                    targets = self.get_tuple(sub_node.targets[0])
                    values = self.get_tuple(sub_node.value)
                else:
                    targets = [sub_node.targets[0]]
                    values = [sub_node.value]
                for i in range(len(targets)):
                    if (type(targets[i]) == ast.Name) and \
                            (targets[i].id not in first_assigns) and \
                            not self.check_thread_assign(values[i]):
                        first_assigns += [targets[i].id]
                remove_nodes += [sub_node]
            else:
                remove_nodes += [sub_node]
            remove_nodes += nodes_to_add
            added_nodes += nodes_to_add

        if remove_nodes:
            main_function.body = [ast.Global(names=first_assigns)] + remove_nodes
        else:
            main_function.body = [ast.Pass()]
        node.body = leave_nodes + [main_function]

        return first_assigns, added_nodes

    # :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: get_tuple
    @staticmethod
    def get_tuple(node):
        parts = []
        for part in node.elts:
            parts += [part]
        return tuple(parts)

    # :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: edit_functions
    def edit_functions(self, node, function_name, function_globals=None):
        if function_globals is None:
            function_globals = []

        line_integer = 0
        if hasattr(node, 'body'):
            for sub_node in node.body:
                # print '\n' + str(type(sub_node)) + '\n'

                if type(sub_node) == ast.Global:
                    function_globals += sub_node.names

                if hasattr(sub_node, 'body') or hasattr(sub_node, 'orelse'):
                    self.edit_functions(sub_node, function_name, function_globals)

                elif type(sub_node) == ast.Assign:
                    self.change_assign(node.body, sub_node, function_name, function_globals, line_integer)

                line_integer += 1
        line_integer = 0
        if hasattr(node, 'orelse'):
            for sub_node in node.orelse:
                # print '\n' + str(type(sub_node)) + '\n'

                if type(sub_node) == ast.Global:
                    function_globals += sub_node.names

                if hasattr(sub_node, 'body') or hasattr(sub_node, 'orelse'):
                    self.edit_functions(sub_node, function_name, function_globals)

                elif type(sub_node) == ast.Assign:
                    self.change_assign(node.orelse, sub_node, function_name, function_globals, line_integer)

                line_integer += 1

    # :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: check_thread_assign
    def change_assign(self, lst, sub_node, function_name, function_globals, line_integer):
        self.assign_count += 1
        if type(sub_node.targets[0]) == ast.Tuple:
            targets = self.get_tuple(sub_node.targets[0])
            values = self.get_tuple(sub_node.value)
        else:
            targets = [sub_node.targets[0]]
            values = [sub_node.value]

        for i in range(len(targets)):
            thread_assign = False
            if (type(values[i]) == ast.Call) and \
                    (((type(values[i].func) == ast.Name) and (values[i].func.id in ('Thread', 'ClusterThread'))) or
                     ((type(values[i].func) == ast.Attribute) and
                      (values[i].func.attr in ('Thread', 'ClusterThread')))):
                # print 'found thread assign'
                thread_assign = True

            if (not thread_assign) and (sub_node not in self.global_first_assigns):
                name, place = self.get_subscript(targets[i])
                if (name in function_globals) or \
                        ((function_name == 'main') and (name in self.first_assigns)) or \
                        ((name in self.first_assigns) and place):
                    if not place:
                        new_node = ast.Expr(
                            value=ast.Call(
                                func=ast.Attribute(
                                    value=ast.Name(
                                        id='ClusterGlobal', ctx=ast.Load()), attr='change_global',
                                    ctx=ast.Load()),
                                args=[ast.Name(id='globals_table', ctx=ast.Param()),
                                      ast.Name(id='my_server', ctx=ast.Load()),
                                      ast.Attribute(value=ast.Name(id=function_name, ctx=ast.Load()),
                                                    attr='__module__', ctx=ast.Load()), ast.Str(s=name),
                                      ast.Name(id=name, ctx=ast.Load())], keywords=[], starargs=None,
                                kwargs=None))
                    else:
                        # print str(type(place)) + ' ' + str(place)
                        # test_parse = ast.parse(str(place))
                        # print str(test_parse) + str(ast.dump(test_parse))
                        place_ast = ast.List(elts=place, ctx=ast.Load())
                        # print 'place_ast ' + str(place_ast) + str(ast.dump(place_ast))
                        subscript = targets[i]
                        new_node = ast.Expr(
                            value=ast.Call(
                                func=ast.Attribute(
                                    value=ast.Name(
                                        id='ClusterGlobal', ctx=ast.Load()), attr='change_in_global',
                                    ctx=ast.Load()), args=[ast.Name(id='globals_table', ctx=ast.Param()),
                                                           ast.Name(id='my_server', ctx=ast.Load()),
                                                           ast.Attribute(value=ast.Name(id=function_name,
                                                                                        ctx=ast.Load()),
                                                                         attr='__module__', ctx=ast.Load()),
                                                           ast.Str(s=name), place_ast, subscript],
                                keywords=[], starargs=None, kwargs=None))
                    lst.insert(line_integer + 1, new_node)

    # :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: check_thread_assign
    @staticmethod
    def check_thread_assign(value):
        if (type(value) == ast.Call)and (type(value.func) == ast.Name) and (value.func.id == 'Thread'):
            return True
        return False

    # :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: get_subscript
    def get_subscript(self, node):
        if type(node) == ast.Name:
            return node.id, []
        target, subscript_index = self.get_subscript(node.value)
        add_index = node.slice.value
        # print 'add_index ' + str(type(add_index))
        return target, subscript_index + [add_index]

    # :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: get_body_and_orelse
    def get_body_and_orelse(self, node):
        orelse = []
        if not hasattr(node, 'orelse'):
            if not hasattr(node, 'body'):
                return [node]
            orelse += node.body
            for n in node.body:
                if hasattr(n, 'orelse'):
                    orelse += self.get_body_and_orelse(n)
            return orelse
        for n in node.orelse:
            orelse += self.get_body_and_orelse(n)
        return orelse

    # :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: get_my_functions
    @staticmethod
    def get_my_functions(node):
        my_functions = []
        if hasattr(node, 'body'):
            for sub_node in node.body:
                if type(sub_node) == ast.FunctionDef:
                    my_functions += [sub_node.name]
        return my_functions

    # :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: organize_module
    @staticmethod
    def organize_module(node):
        import_nodes = []
        function_nodes = []
        other_nodes = []
        for sub_node in node.body:
            if type(sub_node) in (ast.Import, ast.ImportFrom):
                import_nodes += [sub_node]
            elif type(sub_node) == ast.FunctionDef:
                function_nodes += [sub_node]
            else:
                other_nodes += [sub_node]
        node.body = import_nodes + function_nodes + other_nodes
        ast.fix_missing_locations(node)


# :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
class FunctionChanger(ast.NodeTransformer):
    def __init__(self, function_name):
        self.function_name = function_name
        self.count = 0

    def visit(self, node):
        if (type(node) == ast.Call) and \
                (((type(node.func) == ast.Name) and (node.func.id in ('Thread', 'ClusterThread'))) or
                 ((type(node.func) == ast.Attribute) and (node.func.attr in ('Thread', 'ClusterThread')))):
            self.count += 1
            # print self.count
            node.args = [ast.Name(id='my_server', ctx=ast.Load()), ast.Attribute(
                value=ast.Name(id=self.function_name, ctx=ast.Load()), attr='__module__',
                ctx=ast.Load())] + node.args

        # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        method = 'visit_' + node.__class__.__name__
        visitor = getattr(self, method, self.generic_visit)
        return visitor(node)


# |||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
def parse(file_path):
    with open(file_path, 'r') as f:
        try:
            tree = ast.parse(f.read())
        except Exception, e:
            return None, 'Compile time error in the code: ' + str(e)

    if tree:
        tree_visitor = TreeVisitor()
        # try:
        tree_visitor.visit(tree)
        # except Exception, e:
        #     return None, 'Error with parsing: ' + str(e)
        # print 'assign_count: ' + str(tree_visitor.assign_count)

        with open('add_for_cluster.py', 'r') as f:
            add_code = ast.parse(f.read()).body
        for j in range(len(add_code)):
            tree.body.insert(0, add_code[len(add_code) - j - 1])

        tree_visitor.organize_module(tree)

        code = astor.to_source(tree)
        return code, None


# quick_sort
# caesar
# test_code
# print parse('test_code.py')[0]

# code, error = parse('test_files/caesar2.py')
# print code
# print error
# with open('caesar2.py', 'w') as f:
#     f.write(code)
