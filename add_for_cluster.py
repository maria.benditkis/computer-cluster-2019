# code that is added to the original code
from cluster_threading import *
import threading


def set_important_variables(table):
    global globals_table
    globals_table = table


def call_function(my_server, function_name, args):
    logger.info('calling ' + function_name + str(args))
    eval(function_name)(my_server, *args)


def change_global(variable_name, variable):
    # print 'change_global variable name ' + variable_name
    # print 'change_global variable ' + str(variable)
    logger.debug('changing ' + variable_name + ' to ' + str(variable))
    logger.debug('before lock')
    if variable_name == 'max_true_message':
        print 'changing max_true_message'
    with globals_lock:
        globals()[variable_name] = variable
    if variable != globals()[variable_name]:
        print 'very big problems!!!'
    # print globals()['ls']
    logger.info('change_global globals()[%s] = %s' % (variable_name, globals()[variable_name]))
    # print 'change_global globals()[%s] = %s' % (variable_name, globals()[variable_name])
    logger.debug('after lock')
    if globals_table:
        update_globals_table(globals_table, variable_name, None, variable)


def change_in_global(variable_name, place, variable):
    # print 'change_in_global variable name ' + variable_name
    # print 'change_in_global place ' + str(place)
    # print 'change_in_global variable ' + str(variable)
    str_place = ''
    for p in place:
        str_place += '[' + repr(p) + ']'
    logger.debug('before lock')
    logger.debug('change_in_global before change in global exec')
    # print 'change_in_global change in global  ' + str(globals()[variable_name])
    with globals_lock:
        exec('globals()[variable_name]' + str_place + ' = variable')
    logger.debug('change_in_global after change in global exec')
    logger.debug('after lock')
    if globals_table:
        update_globals_table(globals_table, variable_name, place, variable)


globals_lock = threading.Lock()
globals_table = None
logger = logging.getLogger(__name__)
