from threading import Thread
import json
from collections import deque
from time import time, sleep
import datetime


def translate(message, key):
    alphabet_upper = deque(list('ABCDEFGHIJKLMNOPQRSTUVWXYZ'))
    alphabet_upper.rotate(-key)
    alphabet_lower = deque(list('abcdefghijklmnopqrstuvwxyz'))
    alphabet_lower.rotate(-key)
    translated = ''

    for symbol in message:
        if symbol.isalpha():
            if symbol.isupper():
                translated += alphabet_upper[ord(symbol) - ord('A')]
            else:
                translated += alphabet_lower[ord(symbol) - ord('a')]
        else:
            translated += symbol
    return translated


def decode(key):
    global encoded_message, found_message, max_true_message, max_true_count

    print 'decoding with key ' + str(key)

    decoded_message = translate(encoded_message, -key)
    words = decoded_message.split(' ')
    count_true = 0

    with open('words_dictionary.json') as json_file:
        data = json.load(json_file)

    for word in words:
        if word.lower() in data:
            count_true += 1
    if count_true > max_true_count:
        max_true_count = count_true
        max_true_message = decoded_message


def runThreads():
    threads = []

    for decode_key in range(1, 26):
        decode_thread = Thread(target=decode, args=[decode_key])
        threads += [decode_thread]
        decode_thread.start()

    for t in threads:
        t.join()


start_time = time()
my_message = 'Hello my name is Maria a b c d e f g h i j k l m n o p q r s t u v w x y z'
encode_key = 7

encoded_message = translate(my_message, encode_key)
max_true_message = ''
max_true_count = 2

print my_message
print 'Your encoded text is:'
print encoded_message

runThreads()

print '\nYour decoded text is: ' + max_true_message
print 'max_true_count ' + str(max_true_count)
found_message = my_message == max_true_message
decoded_message = max_true_message
print str(datetime.timedelta(seconds=(time() - start_time)))[:-3]
