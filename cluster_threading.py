from threading import Thread, Lock
import pickle
import logging
from PyQt4 import QtGui


# replaces the Thread class
class ClusterThread(object):
    def __init__(self, server, path, group=None, target=None, name=None, args=(), kwargs=None, daemon=False):
        self.server = server
        self.path = path
        self.group = group
        self.target = target
        self.name = name
        self.args = args
        self.kwargs = kwargs
        self.daemon = daemon
        self.new_thread = None

    # creates a thread that sends a request to start a function
    def start(self):
        self.new_thread = Thread(target=self.start_thread)
        self.new_thread.start()

    # sends a request to start a function
    def start_thread(self):
        path = str(self.path)
        name = str(self.target.__name__)
        args = []
        for arg in self.args:
            args += [pickle.dumps(arg)]
        args = ',, '.join(args)
        logger.info(path + '.' + name + '(' + str(self.args) + ') start')
        num_threads_computers = self.server.num_threads_computers
        address_ip = min(num_threads_computers, key=num_threads_computers.get)
        if address_ip == self.server.address[0]:
            self.server.worker(None, path, name, self.args)
        else:
            self.server.my_clients.clients[address_ip].send_message(
                'call function in thread ::: ' + path + ',,, ' + name + ',,, ' + args)
        logger.info(path + '.' + name + '(' + str(self.args) + ') end')

    def join(self):
        self.new_thread.join()


# allows to change global parameters in other computers
class ClusterGlobal(object):
    @staticmethod
    # sends a message that a global changed
    def change_global(globals_table, server, path, variable_name, variable):
        message = 'change global ::: ' + str(path) + ',,, ' + str(variable_name) + ',,, ' + pickle.dumps(variable)
        logger.info('change global ::: ' + str(path) + ',,, ' + str(variable_name) + ',,, ' + repr(variable))
        server.my_clients.send_to_everyone(message)
        if globals_table:
            update_globals_table(globals_table, variable_name, None, variable)

    @staticmethod
    # sends a message that something in a global changed (for example in a list)
    def change_in_global(globals_table, server, path, variable_name, place, variable):
        message = 'change in global ::: ' + str(path) + \
                  ',,, ' + variable_name + \
                  ',,, ' + pickle.dumps(place) + \
                  ',,, ' + str(pickle.dumps(variable))
        server.my_clients.send_to_everyone(message)
        if globals_table:
            update_globals_table(globals_table, variable_name, place, variable)


# updates the global table in the run tab
def update_globals_table(table, variable_name, place, variable):
    # print 'update_globals_table'
    row_count = table.rowCount()
    for row in range(row_count):
        item = table.item(row, 0)
        if item and item.text() == variable_name:
            global_row = row
            break
    else:
        global_row = row_count
        table.insertRow(row_count)
        table.setItem(row_count, 0, QtGui.QTableWidgetItem(variable_name))
        table.setItem(row_count, 1, QtGui.QTableWidgetItem())
        table.setItem(row_count, 2, QtGui.QTableWidgetItem())

    if not place:
        table.item(global_row, 1).setText(str(type(variable)))
        table.item(global_row, 2).setText(repr(variable))
    else:
        str_place = ''
        for p in place:
            str_place += '[' + repr(p) + ']'
        var = eval(str(table.item(global_row, 2).text()))
        exec('var' + str_place + ' = variable')
        table.item(global_row, 2).setText(repr(var))


logger = logging.getLogger(__name__)
